import base64
import importlib.resources as pkg_resources
import zlib
from collections import OrderedDict
from time import time

import numpy as np
from flask import make_response
from jinja2 import Template
from labthings import current_action, fields, find_component, find_extension
from labthings.schema import Schema
from labthings.views import ActionView, PropertyView, View

from . import templates
from .Command import ValidationError


class RunScriptView(ActionView):
    args = {
        "script": fields.String(
            required=True,
            description="Script content. Plaintext or gzip compressed and b64 encoded",
        ),
        "validateOnly": fields.Boolean(
            missing=False,
            required=False,
            description="Do not execute, only run syntax check",
        ),
        "compressed": fields.Boolean(
            missing=False,
            required=False,
            description="Is the content of script compressed?",
        ),
    }

    def post(self, args):
        """
        Execute or validate script
        """
        action = current_action()
        action.data["executing_line"] = 0
        start = time()
        extension = find_extension("org.openflexure.scripting")
        if "compressed" in args and args["compressed"]:
            script = zlib.decompress(base64.b64decode(args["script"])).decode()
        else:
            script = args["script"]

        for line_number, line in enumerate(script.splitlines()):
            if action.stopped:
                return f"Aborted before line {line_number + 1}"
            line = line.strip()
            if line == "":
                continue
            processed_args = []
            processed_kwargs = {}
            action.data["executing_line"] = line_number
            parts = line.split(" ")
            command = parts[0]
            for part in parts[1:]:
                if "=" in part:
                    key = part.split("=")[0]
                    value = part.split("=")[1]
                    if key in processed_kwargs:
                        action.data["result"] = "fail"
                        return f"Syntax error at {line_number + 1}: Duplicate argument {key}"
                    processed_kwargs[key] = value
                else:
                    processed_args.append(part)
            if command not in extension.get_commands():
                action.data["result"] = "fail"
                return f"Command {command} does not exist (line {line_number})"

            try:
                if args["validateOnly"]:
                    extension.get_commands()[command].validate(
                        processed_args, processed_kwargs
                    )
                elif extension.get_commands()[command].run(processed_args, processed_kwargs) is False:
                    action.data["result"] = "fail"
                    return f"Execution error at {line_number + 1}"
            except ValidationError as error:
                action.data["result"] = "fail"
                return f"Syntax error at {line_number + 1}: {error.message}"
        action.data["result"] = "success"
        return f"Completed in {time() - start} s"


class SyntaxHelpView(View):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Set the default representations
        def output_html(data, code, headers=None):
            resp = make_response(data, code)
            resp.headers.extend(headers or {})
            resp.mimetype = "text/html"
            return resp

        self.representations = OrderedDict({"text/html": output_html})

    def get(self):
        """
        HTML page describing syntax and available commands
        """
        templateText = pkg_resources.read_text(templates, "help.html")
        template = Template(templateText)
        extension = find_extension("org.openflexure.scripting")
        return template.render({"commands": extension.get_commands()})
