import json
import logging
import re
import urllib.request
from time import sleep, time

import numpy as np
from flask import current_app as app
from labthings import find_component, find_extension
from labthings.extensions import BaseExtension
from labthings.find import current_labthing, registered_extensions
from labthings.utilities import path_relative_to
from openflexure_microscope.api.utilities.gui import build_gui

from .Command import *
from .ScriptExtensionViews import RunScriptView, SyntaxHelpView


# Create the extension class
class ScriptExtension(BaseExtension):
    _commands = {}

    def __init__(self):
        super().__init__(
            "org.openflexure.scripting",
            version="0.0.1",
            static_folder=path_relative_to(__file__, "static"),
        )

        def gui_func():
            return {"icon": "code", "frame": {"href": self.static_file_url("")}}

        self.add_view(RunScriptView, "/run")
        self.add_view(SyntaxHelpView, "/help")
        self.add_meta("gui", build_gui(gui_func, self))

    def get_commands(self):
        if len(self._commands.items()) > 0:
            return self._commands

        for name, command_definition in self.script_commands.items():
            self._commands[name] = Command(self, name, command_definition)

        with urllib.request.urlopen("http://localhost:5000/api/v2/docs/swagger") as url:
            swagger_raw = json.loads(url.read().decode())["paths"]

        endpoint_url_map = {}
        for rule in app.url_map.iter_rules():
            endpoint_url_map[rule.endpoint] = rule.rule

        for extension in registered_extensions().values():
            if not hasattr(extension, "script_commands"):
                continue
            for name, command_definition in extension.script_commands.items():
                self._commands[name] = Command(extension, name, command_definition)

        for view_data in current_labthing().views:
            (view, urls, endpoint, _) = view_data
            if not view.endpoint in endpoint_url_map:
                logging.error(f"No matching url for endpoint {view.endpoint}")
                continue
            url = endpoint_url_map[view.endpoint]
            if "/static/" in url:
                continue
            url = re.sub(r"<[^/:>]*:([^>]+)>", r"{\1}", url)
            url = url.replace("<", "{").replace(">", "}")
            if not url in swagger_raw:
                logging.error(f"Swagger UI does not contain {url}")
                continue
            if not "post" in swagger_raw[url]:
                continue
            short_name = view.endpoint.split("/")[-1]
            if short_name in self._commands:
                short_name = view.endpoint
            self._commands[short_name] = Command.from_view(
                view, short_name, swagger_raw[url]["post"]
            )
            logging.debug(f"Registered commmand {short_name} for {url}")

        return self._commands

    def move(self, x, y, z):
        microscope = find_component("org.openflexure.microscope")
        if microscope.stage is None:
            return False

        return microscope.stage.move_rel([x, y, z])

    def image(self, filename=None, **kwargs):
        microscope = find_component("org.openflexure.microscope")
        return microscope.capture(
            filename=filename,
            temporary="temporary" in kwargs and kwargs["temporary"] == 1,
            use_video_port="video_port" in kwargs and kwargs["video_port"] == 1,
            # resize=resize,
            # bayer=args.get("bayer"),
            # annotations=args.get("annotations"),
            # tags=args.get("tags"),
        )

    def delay(self, time):
        sleep(float(time))
        return True

    script_commands = {
        "move": {
            "function": move,
            "description": "Move stage",
            "args": ["int:X-steps", "int:Y-steps", "int:Z-steps"],
            "kwargs": {},
        },
        "image": {
            "function": image,
            "description": "Save image",
            "args": ["?str:Filename"],
            "kwargs": {
                "temporary": "?int:Is temporary (0,1)",
                "video_port": "?int:Use video port (0,1)",
            },
        },
        "delay": {
            "function": delay,
            "description": "Sleep",
            "args": ["float:Sleep time in seconds"],
            "kwargs": {},
        },
    }
