import json
import logging
import re


class ValidationError(Exception):
    def __init__(self, message):
        self.message = message


class Command:
    def __init__(self, extension, name, definition=None, source="definition"):
        self.argument_definitions = []
        self.keyword_argument_definitions = {}
        self.extension = extension
        self.name = name
        self.source = source
        self.description = ""
        self.extension_function = None

        if definition is not None:
            for arg in definition["args"]:
                self.argument_definitions.append(self._parse_definition_string(arg))

            for key, arg in definition["kwargs"].items():
                self.keyword_argument_definitions[key] = self._parse_definition_string(
                    arg
                )

            self.extension_function = definition["function"]
            self.description = definition["description"]

    def run(self, arguments=[], keyword_arguments={}):
        try:
            self.validate(arguments, keyword_arguments)
            (
                converted_arguments,
                converted_keyword_arguments,
            ) = self._convert_argument_types(arguments, keyword_arguments)
        except ValueError:
            return False
        if (
            self.extension_function(
                self.extension, *converted_arguments, **converted_keyword_arguments
            )
            is False
        ):
            return False
        return True

    def validate(self, arguments, keyword_arguments):
        """
        Check if set of arguments and keyword arguments is a valid call
        """
        if len(arguments) > len(self.argument_definitions):
            raise ValidationError(
                f"Excess arguments: expected max {len(self.argument_definitions)}, received {len(arguments)}"
            )

        for i, argument_definition in enumerate(self.argument_definitions):
            if len(arguments) <= i and not argument_definition["optional"]:
                raise ValidationError(
                    f"Missing positional argument {i} {argument_definition['description']}"
                )

            if len(arguments) > i and not self._check_argument(
                argument_definition, arguments[i]
            ):
                raise ValidationError(
                    f"Type conversion to {argument_definition['type']} failed for {arguments[i]}"
                )

        for key, argument_definition in self.keyword_argument_definitions.items():
            if not argument_definition["optional"] and not key in keyword_arguments:
                raise ValidationError(f"Missing keyword argument {key}")

            if key in keyword_arguments and not self._check_argument(
                argument_definition, keyword_arguments[key]
            ):
                raise ValidationError(
                    f"Type conversion to {argument_definition['type']} failed for {keyword_arguments[key]} ({key})"
                )

        for key in keyword_arguments:
            if key not in self.keyword_argument_definitions:
                raise ValidationError(f"Unknown argument {key}")

        return True

    def _convert_argument_type(self, argument_definition, argument):
        if argument_definition["type"] == "int":
            return int(argument)
        if argument_definition["type"] == "float":
            return float(argument)
        if argument_definition["type"] == "str":
            return str(argument)
        if argument_definition["type"] == "array":
            return list(json.loads(argument))
        if argument_definition == "object":
            return json.loads(argument)
        if argument_definition == "dict":
            json_data = json.loads(argument)
            if not isinstance(json_data, dict):
                raise ValueError("Not a dict")
            return json_data
        if argument_definition["type"] == "boolean" or argument_definition == "bool":
            if argument.lower() == "false" or argument == "0":
                return False
            elif argument.lower() == "true" or argument == "":
                return True
            else:
                raise ValueError(f"Invalid value for boolean {argument}")
        # if default, try to guess
        if argument.startswith("["):
            return list(json.loads(argument))
        if argument.startswith("{"):
            return json.loads(argument)
        return argument

    def _check_argument(self, argument_definition, argument):
        try:
            self._convert_argument_type(argument_definition, argument)
        except (ValueError, TypeError):
            return False
        return True

    def _convert_argument_types(self, arguments, keyword_arguments):
        converted_arguments = []
        converted_keyword_arguments = {}
        for i, argument in enumerate(arguments):
            converted_arguments.append(
                self._convert_argument_type(self.argument_definitions[i], argument)
            )

        for key, argument in keyword_arguments.items():
            converted_keyword_arguments[key] = self._convert_argument_type(
                self.keyword_argument_definitions[key], argument
            )

        return (converted_arguments, converted_keyword_arguments)

    def _parse_definition_string(self, definitionString):
        """
        Parse definition string format to a standard dict for processing

        :param definitionString: string to be parsed
        :type definitionString: str
        """
        match = re.match("^\??([a-zA-Z]+):", definitionString)
        if match:
            argType = match.group(1)
        else:
            argType = "any"

        split = definitionString.split(":", 1)
        if len(split) == 2:
            description = split[1]
        else:
            description = definitionString.strip("?")

        return {
            "optional": definitionString.startswith("?"),
            "type": argType,
            "description": description,
        }

    @staticmethod
    def _run_view_method(view, *args, **kwargs):
        """Helper to execute the view method, passing appropriate arguments in args

        Args:
            view (View): the related view (will be passed as extension by Command)
        """
        view_instance = view()

        if len(kwargs):
            view_instance.post(*args, kwargs)
        else:
            view_instance.post(*args)

    @staticmethod
    def _parse_swagger_prop(prop_name, prop_definition, required_props):
        if "type" in prop_definition and not isinstance(prop_definition["type"], list):
            prop_type = (
                prop_definition["type"]
                .replace("number", "float")
                .replace("string", "str")
                .replace("integer", "int")
            )
            if prop_definition["type"] == "number" and "format" in prop_definition:
                if "int" in prop_definition["format"]:
                    prop_type = "int"
        else:
            prop_type = "any"

        if "description" in prop_definition:
            prop_description = prop_definition["description"]
        else:
            prop_description = ""

        if "example" in prop_definition:
            prop_description += " (e.g. " + str(prop_definition["example"]) + ")"

        return {
            "optional": prop_name in required_props,
            "type": prop_type,
            "description": prop_description,
        }

    @staticmethod
    def from_view(view, name, swagger_description):
        """
        :param view: API view which this command should implement
        :type view: View
        """
        description = swagger_description["description"]
        keyword_args = {}
        only_arg = False
        if (
            "requestBody" in swagger_description
            and "content" in swagger_description["requestBody"]
        ):
            request_content = swagger_description["requestBody"]["content"]
            if len(request_content.keys()) > 1:
                logging.warn(
                    f"Command {name} supports multiple request body types, only first one used here"
                )
            request_content = request_content[list(request_content.keys())[0]]

            if "schema" in request_content:
                schema = request_content["schema"]

                if "required" in schema:
                    required_props = schema["required"]
                else:
                    required_props = []

                if "properties" in schema:
                    only_arg = False
                    for prop_name, prop_definition in schema["properties"].items():
                        keyword_args[prop_name] = Command._parse_swagger_prop(
                            prop_name, prop_definition, required_props
                        )
                else:
                    only_arg = [Command._parse_swagger_prop("", schema, [""])]
        command = Command(view, name, None, "auto")
        command.extension_function = Command._run_view_method
        command.description = description
        command.argument_definitions = only_arg if only_arg else []
        command.keyword_argument_definitions = keyword_args
        return command
