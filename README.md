# OFM Scripting extension
## Description
This extension supports simple scripts which are run synchronously on the server.
Commands are generated from defined API endpoints and every extension can define additional 
scripting commands by setting `script_commands` class property on the extension class
(see `ScriptExtension/__init__.py` for how this is done).

It has an intuitive scripting syntax of the form `command [arg1] [arg2] ... [keyword=value] [keyword2=value2] ...`,
validation logic which checks if all given commands exist and have all required parameters and a UI which will
highlight any validation issues and show progress during execution. Scripts can be either entered directly in
the UI or uploaded as text files. All available commands (which will depend on other installed
extensions) are listed on the `Syntax help` page linked in the UI.

There is no support for variables or any other constructs beyond a list of commands to be executed,
if you need more complex processing, there are libraries in python and matlab (and possibly others),
which will use the API directly. It is possible to combine these with this extension by passing scripts
as text through the API.

## Installation
To install, clone/copy the content of this repository to `/var/openflexure/extensions/microscope_extensions`.

For more information about extensions, see the [API documentation](https://openflexure-microscope-software.readthedocs.io/en/master/plugins.html) and the [handbook](https://openflexure.gitlab.io/microscope-handbook/develop-extensions/index.html).
